import { ReduxCounterAppPage } from './app.po';

describe('redux-counter-app App', () => {
  let page: ReduxCounterAppPage;

  beforeEach(() => {
    page = new ReduxCounterAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
