/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core'

import { CounterService } from '../../services/counter';
import { Counter } from '../../stores';

@Component({
   selector: 'total-counters',
   templateUrl: './total-counters.component.html',
   styleUrls: ['./total-counters.component.less']
})
export class TotalCountersComponent implements OnInit, OnDestroy {

  totalText: string;
  private _counterSubscription: Subscription;

  constructor(
    private _counterService: CounterService,
    private _translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this._counterSubscription = this._counterService.counters$.subscribe(counters => {
      const total = counters.length;
      if (total > 1) {
        this._translateService.get('component.total-counters.multiple-counters', {total: total}).subscribe(text => {
          this.totalText = text;
        });
      } else if (total === 1) {
        this._translateService.get('component.total-counters.single-counter').subscribe(text => {
          this.totalText = text;
        });
      } else if (total === 0) {
        this._translateService.get('component.total-counters.no-counters').subscribe(text => {
          this.totalText = text;
        });
      }
    });
  }

  ngOnDestroy(): void {
    this._counterSubscription.unsubscribe();
  }

}
