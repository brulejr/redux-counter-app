/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import {
  AddCounterAction,
  AppState,
  Counter,
  DecrementCounterAction,
  IncrementCounterAction,
  RemoveAllCountersAction,
  RemoveCounterAction,
  ResetCounterAction } from '../../stores';


@Injectable()
export class CounterService {

  counters$: Observable<Counter[]>;

  constructor(private _store: Store<AppState>) {
    this.counters$ = _store.select('counters');
  }

  addCounter(): void {
    this._store.dispatch(new AddCounterAction());
  }

  decrementCounter(index: number) {
    this._store.dispatch(new DecrementCounterAction(index));
  }

  incrementCounter(index: number): void {
    this._store.dispatch(new IncrementCounterAction(index));
  }

  removeAllCounters(): void {
    this._store.dispatch(new RemoveAllCountersAction());
  }

  removeCounter(index: number) {
    this._store.dispatch(new RemoveCounterAction(index));
  }

  resetCounter(index: number) {
    this._store.dispatch(new ResetCounterAction(index));
  }

}
