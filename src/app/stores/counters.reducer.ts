/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
import { Action } from '@ngrx/store';

import { AppState } from './app.state';
import { Counter } from './counter.state';

const ADD_COUNTER = 'counter/ADD_COUNTER'
const DECREMENT_COUNTER = 'counter/DECREMENT_COUNTER'
const INCREMENT_COUNTER = 'counter/INCREMENT_COUNTER'
const REMOVE_ALL_COUNTERS = 'counter/REMOVE_ALL_COUNTERS';
const REMOVE_COUNTER = 'counter/REMOVE_COUNTER'
const RESET_COUNTER = 'counter/RESET_COUNTER'

export const COUNTER_INITIAL_STATE = <Counter>{
  counter: 0
}

export const INITIAL_STATE = [
]

export class AddCounterAction implements Action {
  readonly type = ADD_COUNTER;
}

export class DecrementCounterAction implements Action {
  readonly type = DECREMENT_COUNTER;
  constructor(public index: number) { }
}

export class IncrementCounterAction implements Action {
  readonly type = INCREMENT_COUNTER;
  constructor(public index: number) { }
}

export class RemoveAllCountersAction implements Action {
  readonly type = REMOVE_ALL_COUNTERS;
}

export class RemoveCounterAction implements Action {
  readonly type = REMOVE_COUNTER;
  constructor(public index: number) { }
}

export class ResetCounterAction implements Action {
  readonly type = RESET_COUNTER;
  constructor(public index: number) { }
}

export type Actions
  = AddCounterAction
  | DecrementCounterAction
  | IncrementCounterAction
  | RemoveAllCountersAction
  | RemoveCounterAction
  | ResetCounterAction;

export function countersReducer(state: Counter[] = INITIAL_STATE, action: Actions) {
  switch (action.type) {
    case ADD_COUNTER:
      return [
        ...state,
        Object.assign({}, COUNTER_INITIAL_STATE, { timestamp: Date.now() })
      ];
    case INCREMENT_COUNTER:
      return [
        ...state.slice(0, action.index),
        { counter: state[action.index].counter + 1, timestamp: Date.now() },
        ...state.slice(action.index + 1)
      ]
    case DECREMENT_COUNTER:
      return [
        ...state.slice(0, action.index),
        { counter: state[action.index].counter - 1, timestamp: Date.now() },
        ...state.slice(action.index + 1)
      ]
    case REMOVE_ALL_COUNTERS:
      return INITIAL_STATE;
    case REMOVE_COUNTER:
      return [
        ...state.slice(0, action.index),
        ...state.slice(action.index + 1)
      ]
    case RESET_COUNTER:
      return [
        ...state.slice(0, action.index),
        Object.assign({}, COUNTER_INITIAL_STATE, { timestamp: Date.now() }),
        ...state.slice(action.index + 1)
      ]

    default:
      return state;
  }
};
